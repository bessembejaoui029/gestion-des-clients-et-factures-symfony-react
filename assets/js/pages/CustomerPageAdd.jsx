import React, {useEffect, useState} from 'react';
import Field from "../components/forms/Field";
import {Link} from "react-router-dom";
import axios from 'axios';
import CustomersApi from "../services/CustomersApi";
import {toast} from "react-toastify";
import FormContentLoader from "../components/loaders/FormContentLoader";


const CustomerPageAdd = (props) => {
    console.log(props);
    const {id="new"} = props.match.params;

    const [customer, setCustomer]=useState({
        lastname : '',
        firstname : '',
        email : '' ,
        company : '',
    });

    const [error,setError]=useState({
        lastname : '',
        firstname : '',
        email : '' ,
        company : '',
    })
    const [editing,setEditing]=useState(false);
    const [loading,setLoading]=useState(false);
    const fetchCustomer = async (id)=> {
        try {
            const {firstname ,lastname ,email,company , user } = await CustomersApi.find(id);
            setCustomer({firstname , lastname , email ,company});
            setLoading(false);
        }catch (error){
            console.log(error.response);
            toast.error("le client n'a pas pu etre chargé");
            props.history.replace("/customers");
        }
    }


    useEffect(()=>{
        if(id !== "new") {
            setLoading(true);
            setEditing(true);
            fetchCustomer(id);

        }
    },[id]);

    const handelChange = (event) => {
      const  {name , value} = event.currentTarget ;
      setCustomer({...customer,[name] : value});
    }
const handelSubmit = async (event)=>{
      event.preventDefault();
      try{
          setError({});
          if(editing){
              const response = await CustomersApi.update(id,customer);
              toast.success("Le client a bien ete modifier")
              console.log(response.data);

          }else{
              const response = await CustomersApi.create(customer);
              toast.success("Le client a bien ete ajouter")
              props.history.replace("/customers");
          }


        console.log(response.data);
      }catch (error){
          if(error.response.data.violations){
              const apiErrors ={};
              error.response.data.violations.forEach(violation=>{
                      apiErrors[violation.propertyPath] = violation.message;
                  }
              );
              setError(apiErrors);
              toast.error("des erreurs dans votre formulaire !");
              console.log(apiErrors);
          }
      }
      console.log(customer);
}
    return(
        <>
            {!editing && <h1>creation un client</h1> || <h1>Modification du client</h1>}
            {loading && <FormContentLoader/>}
            {!loading && <form onSubmit={handelSubmit}>
                <Field error={error.lastname} value={customer.lastname} onChange={handelChange} name="lastname" label="nom de famille" placeholder="Nom du famille du client"/>
                <Field error={error.firstname} value={customer.firstname} onChange={handelChange} name="firstname" label="Prenom" placeholder="Prenom du client"/>
                <Field error={error.email} value={customer.email} onChange={handelChange} name="email" label="Email" placeholder="Adresse email du client" type="email"/>
                <Field error={error.company} value={customer.company} onChange={handelChange} name="company" label="Entreprise" placeholder="Entreprise du client"/>
                <div className="form-group">
                    <button type="submit" className="btn btn-success">Enregistrer</button>
                    <Link to="/customers" className="btn btn-link">Retourner a la liste</Link>
                </div>
            </form>}

        </>
    )
}

export default CustomerPageAdd;