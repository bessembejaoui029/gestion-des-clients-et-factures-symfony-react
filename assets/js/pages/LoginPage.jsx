import React, {useContext, useState} from 'react';
import AuthApi from "../services/AuthApi";
import AuthContext from "../context/AuthContext";
import Field from "../components/forms/Field";
import {toast} from "react-toastify";


const LoginPage =({history})=>{
    console.log(history);
    const [credentials, setCredentials]=useState({
        email : "",
        password : ""
    })
    const [error,setError]=useState("");

    const {setIsAuthenticated} = useContext(AuthContext);

    const handelChange=(event)=>{

        const {value,name} = event.currentTarget;
        setCredentials({...credentials,[name]:value})

    }


    const handelSubmit= async (event) =>{

    event.preventDefault();

    try {

    await AuthApi.AuthApi(credentials);
    setError("")  ;
        setIsAuthenticated(true);
        toast.success("Vous etes desormais connecté!");
    history.replace("/customers");

    }catch (error) {
       console.log(error.response)
        setError("aucun compte ne possede pas cette adresse email ou les informations ne corresponde pas")
        toast.error("Une erreur est survenue");
    }

    }


    return(
        <>
            <h1>Connexion a l'application</h1>
            <form onSubmit={handelSubmit}>
                <Field
                    name="email"
                    value={credentials.email}
                    onChange={handelChange}
                    label="Adresse Email"
                    type="email"
                    placeholder="Adresse Email de connexion"
                    error={error}
                />
                <Field
                    name="password"
                    label="Mot de passe"
                    onChange={handelChange}
                    value={credentials.password}
                    type="password"
                    placeholder="Mot de passe de connexion"
                    error=""
                />
                <div className="form-group">
                    <button type="submit" className="btn btn-success">Je me connect</button>
                </div>
            </form>
        </>
    )
}

export default LoginPage;