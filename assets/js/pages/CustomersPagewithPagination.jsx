import React, {useEffect, useState} from "react";
import axios from 'axios';

import Pagination from "../components/Pagination";


const CustomersPagewithPagination =(props)=>{
    const [customers,setCustomers]=useState([]);
    const [totalItems,setTotalItems] =useState(0);
    const [currentPage,setCurrentPage] = useState(1);
    const [loading,setLoading]=useState(true);
    const itemPerPage = 2;


    useEffect(() => {
        axios.get(`https://localhost:8000/api/customers?pagination=true&pageitem=${itemPerPage}&page=${currentPage}`)
            .then(response => {
                setCustomers(response.data['hydra:member'])
                setTotalItems(response.data['hydra:totalItems'])
                setLoading(false)
            })
            .catch(error => console.log(error.response));
    },[currentPage]);


    const handleDelete= id =>{
        const oroginalsCustomer=[...customers];
        setCustomers(customers.filter(customer=> customer.id !== id));
        axios.delete("https://localhost:8000/api/customers/" + id)
            .then(response => console.log('ok'))
            .catch(error=>{
                setCustomers(oroginalsCustomer);
                console.log(error.response);
            })

    }
    const handleChangePage =(page)=>{
        setCurrentPage(page);
        setLoading(true);
    }



    const paginatedCustomers = Pagination.getData(customers,currentPage,itemPerPage);



    return(
        <>
            <h1>Listes des clients (pagination)</h1>
            <table className="table table-hover">
                <thead>
                <tr>
                    <th>ID.</th>
                    <th>Client</th>
                    <th>Email</th>
                    <th>Entreprise</th>
                    <th className='text-center'>Facture</th>
                    <th className="text-center">Montant total</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    loading && <tr>
                        <td>Chargement ...</td>
                    </tr>
                }
                {
                    !loading &&
                    customers.map(customer=>
                    <tr key = {customer.id}>
                        <td>{customer.id}</td>
                        <td><a href="#">{customer.firstname}{customer.lastname}</a></td>
                        <td>{customer.email}</td>
                        <td>{customer.company}</td>
                        <td className="text-center">
                            <span>{customer.invoices.length}</span>
                        </td>
                        <td className="text-center">{customer.totalAmount.toLocaleString()} euro</td>
                        <td>
                            <button
                                onClick = {() => handleDelete(customer.id)}
                                disabled={customer.invoices.length > 0}
                                className="btn btn-sm btn-danger"
                            >
                                Supprimer
                            </button>
                        </td>
                    </tr>
                )}

                </tbody>
            </table>
            <Pagination currentPage ={currentPage} itemsPerPage={itemPerPage} length={totalItems}
                        onPageChanged={handleChangePage}/>

        </>
    );
}
export default CustomersPagewithPagination;