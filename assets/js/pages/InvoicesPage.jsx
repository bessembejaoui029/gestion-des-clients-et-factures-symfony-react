import React ,{useState,useEffect} from 'react';
import Pagination from "../components/Pagination";
import moment from 'moment';
import InvoicesApi from "../services/InvoicesApi";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import TableLoader from "../components/loaders/TableLoader";

const STATUS_CLASSES = {
    PAID : 'success',
    SENT : 'primary',
    CANCELLED : 'danger'
}
const STATUS_LABELS = {
    PAID : 'Payée',
    SENT : 'Envoyé',
    CANCELLED : 'Annulée'
}

const InvoicesPage =(props)=>{
    const [invoices,setInvoices]=useState([]);
    const [currentPage,setCurrentPage]=useState(1);
    const [search,setSearch]=useState("");
    const [loading,setLoading]=useState(true);
    const itemPerPage = 5;

    const fetchInvoices = async ()=>{
        try {
            const data = await InvoicesApi.findAll();
           setInvoices(data);
           setLoading(false);
        }catch (error) {
            console.log(error.response);
            toast.error("erreur lors de chargement des factures")
        }
    };

    useEffect(()=>{
    fetchInvoices();
    },[])

    const handleDelete= async id => {
        const oroginalsInvoices = [...invoices];
        setInvoices(invoices.filter(invoice => invoice.id !== id));
        try {
            await InvoicesApi.delete(id)
           toast.success("la facture a bien ete supprimé");
            console.log("ok...")
        } catch (error) {
            console.log(error.response)
            toast.error("une erreur est survenue");
            setInvoices(oroginalsInvoices);
        }
    };

    const formtDate=(str)=>moment(str).format('DD/MM/YYYY') ;

    const handelPageChange= page => setCurrentPage(page);

    const filteredInvoices= invoices.filter(i=>
            i.customer.firstname.toLowerCase().includes(search.toLowerCase()) ||
            i.customer.lastname.toLowerCase().startsWith(search.toLowerCase()) ||
        i.amount.toString().toLowerCase().includes(search.toLowerCase()) ||
        STATUS_LABELS[i.status].toLowerCase().includes(search.toLowerCase())
    )
    const handelSearch = ({currentTarget})=>{
        setSearch(currentTarget.value);
        setCurrentPage(1);
    }

    const paginatedInvoices = Pagination.getData(
        filteredInvoices,
        currentPage,
        itemPerPage
    )

    return(
        <>
            <div className="d-flex justify-content-between align-items-center">
                <h1>Listes des factures</h1>
                <Link className="btn btn-primary" to={"/invoices/new"}>Creer une facture</Link>
            </div>

            <div>
                <input onChange={handelSearch} value={search} className="form-control" placeholder="Recherche..."/>
            </div>
            { !loading && <table className='table table-hover'>
                <thead>
                <tr>
                    <th>Numero</th>
                    <th>Client</th>
                    <th className="text-center">Date d'envoi</th>
                    <th className="text-center">Status</th>
                    <th className="text-center">Montant</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {paginatedInvoices.map(invoice=>
                    <tr key={invoice.id}>
                        <td>{invoice.chrono}</td>
                        <td><Link to={"/customers/"+ invoice.customer.id}>{invoice.customer.firstname} {invoice.customer.lastname}</Link></td>
                        <td className="text-center">{formtDate(invoice.sentAt)}</td>
                        <td className="text-center">
                            <span className={"badge badge-"+STATUS_CLASSES[invoice.status]}>{STATUS_LABELS[invoice.status]}</span>
                        </td>
                        <td className="text-center">{invoice.amount.toLocaleString()    }</td>
                        <td>
                            <Link to={"/invoices/" + invoice.id}className="btn btn-sm btn-primary mr-1">Editer</Link>
                            <button onClick={()=>handleDelete(invoice.id)} className='btn btn-sm btn-danger'>Supprimer</button>
                        </td>
                    </tr>

                )}

                </tbody>
            </table>}
            {loading && <TableLoader/>}
            <Pagination
                currentPage={currentPage}
                itemsPerPage={itemPerPage}
                onPageChanged={handelPageChange}
                length={filteredInvoices.length} />
        </>
    )
}

export default InvoicesPage ;