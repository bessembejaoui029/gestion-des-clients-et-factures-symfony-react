import React, {useEffect, useState} from "react";
import axios from 'axios';
import Pagination from "../components/Pagination";
import CustomersApi from "../services/CustomersApi";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import TableLoader from "../components/loaders/TableLoader";


const CustomerPage =(props)=>{
    const [customers,setCustomers]=useState([]);
    const [currentPage,setCurrentPage] = useState(1);
    const [search,setSearch]=useState("");
    const [loading,setLoading]=useState(true);

    const fetchCustomers = async ()=>{
        try{
            const data= await CustomersApi.findAll();
            await setCustomers(data);
            setLoading(false);
        }catch (error) {
            console.log(error.response)
            toast.error("Impossible de charger les clients");
        }
    }

    useEffect(() => {
        fetchCustomers();
    },[]);


    const handleDelete= async id =>{
        const oroginalsCustomer=[...customers];
        setCustomers(customers.filter(customer=> customer.id !== id));
        try {
          await  CustomersApi.delete(id)
            toast.success("Le client a ete bien supprimé");
        }catch (error){
       setCustomers(oroginalsCustomer);
            toast.error("la suppresion de client n'a pas pu fonctionner");
        }
    }

    const handleChangePage =(page)=>{
        setCurrentPage(page);
    }

    const handleSearch = event =>{
        const value = event.currentTarget.value;
        setSearch(value);
        setCurrentPage(1);
    }

    const itemPerPage = 2;
    const filteredCustomers = customers.filter(c=> c.firstname.toLowerCase().includes(search.toLowerCase()) ||
        c.lastname.toLowerCase().includes(search.toLowerCase()) ||
        c.email.toLowerCase().includes(search.toLowerCase()) ||
        c.company.toLowerCase().includes(search.toLowerCase())
    );

    const paginatedCustomers = Pagination.getData(filteredCustomers,currentPage,itemPerPage);


    return(
        <>
            <div className=" mb-3 d-flex justify-content-between align-items-center">
                <h1>Listes des clients</h1>
                <Link className="btn btn-primary" to="/customers/new">Créer un client</Link>
            </div>

            <div className="form-group">
                <input type="text" onChange={handleSearch} value={search}  className="form-control"  placeholder="Rechercher..."/>
            </div>
            {!loading && <table className="table table-hover">
            <thead>
                <tr>
                    <th>ID.</th>
                    <th>Client</th>
                    <th>Email</th>
                    <th>Entreprise</th>
                    <th className='text-center'>Facture</th>
                    <th className="text-center">Montant total</th>
                    <th></th>
                </tr>
            </thead>
              <tbody>
              {paginatedCustomers.map(customer=>
                  <tr key = {customer.id}>
                      <td>{customer.id}</td>
                      <td><Link to={"/customers/"+ customer.id}>{customer.firstname}{customer.lastname}</Link></td>
                      <td>{customer.email}</td>
                      <td>{customer.company}</td>
                      <td className="text-center">
                          <span className='badge badge-primary'>{customer.invoices.length}</span>
                      </td>
                      <td className="text-center">{customer.totalAmount.toLocaleString()} euro</td>
                      <td>
                          <button
                              onClick = {() => handleDelete(customer.id)}
                              disabled={customer.invoices.length > 0}
                              className="btn btn-sm btn-danger"
                          >
                              Supprimer
                          </button>
                      </td>
                  </tr>
              )}

              </tbody>
          </table>}
            {loading && <TableLoader/>}
            {itemPerPage < filteredCustomers.length && (<Pagination currentPage ={currentPage} itemsPerPage={itemPerPage} length={filteredCustomers.length}
                         onPageChanged={handleChangePage}/>)}

        </>
    );
}
export default CustomerPage;