import React, {useState} from 'react';
import Field from "../components/forms/Field";
import {Link} from "react-router-dom";
import axios from "axios";
import UsersApi from "../services/UsersApi";
import {toast} from "react-toastify";


const RegisterPage = ({history}) =>{
    const [user,setUser] = useState({
        firstName : "",
        lastName : "" ,
        email : "" ,
        password: "",
        passwordConfirm : ""
    })
    const [error,setError] = useState({
        firstName : "",
        lastName : "" ,
        email : "" ,
        password: "",
        passwordConfirm : ""
    })
    const handelChange = (event) =>{
       const  {name , value} = event.currentTarget;
       setUser({...user,[name] : value});
    }
    const handelSubmit = async (event) =>{
        event.preventDefault();
        console.log(user);
        const apiErrors ={};
        if(user.password !== user.passwordConfirm){
            apiErrors.passwordConfirm = "votre confirmation de mot de passe ne pas conforme avec le mot de passe originale"
            setError(apiErrors);
            toast.error("des ereurs dan votre formulaire")
            return;
        }
       try {
            const data = await UsersApi.register(user);
            setError({});
            toast.success("vous etse desormais inscrit,vous pouvez vous connectes");
            history.replace("/login")

            console.log(data);
        }catch (error){
           const {violations} = error.response.data;

          if(violations){
           violations.forEach(violation=>{
               apiErrors[violation.propertyPath] = violation.message
           })
              setError(apiErrors);
           }
          toast.error("des ereurs dan votre formulaire")
       }
    }
    return (
        <>
            <h1>Inscription</h1>
            <form onSubmit={handelSubmit}>
                <Field
                    name="firstName"
                    value={user.firstName}
                    onChange={handelChange}
                    error={error.firstName}
                    placeholder="entrer votre nom"
                    label="nom de famille"
                    type="text"/>
                <Field
                    name="lastName"
                    value={user.lastName}
                    onChange={handelChange}
                    error={error.lastName}
                    placeholder="entrer votre prenom"
                    label="prenom"
                    type="text"/>
                <Field
                    name="email"
                    value={user.email}
                    onChange={handelChange}
                    error={error.email}
                    placeholder="entrer votre email"
                    label="email"
                    type="email"/>
                <Field
                    name="password"
                    value={user.password}
                    onChange={handelChange}
                    error={error.password}
                    placeholder="entrer votre mot de passe"
                    label="mot de passe"
                    type="password"/>
                <Field
                    name="passwordConfirm"
                    value={user.passwordConfirm}
                    onChange={handelChange}
                    error={error.passwordConfirm}
                    placeholder="Confirmer votre mot de passe"
                    label="confirmation de mot de passe"
                    type="password"/>
                <div className="form-group">
                    <button type="submit" className="btn btn-success">confirmer</button>
                    <Link to="/login" className= 'btn btn-link'>j'ai deja un compte</Link>
                </div>

            </form>

        </>
    )
}

export default RegisterPage;