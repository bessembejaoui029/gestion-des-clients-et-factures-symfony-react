import React, {useEffect, useState} from "react";
import Field from "../components/forms/Field";
import Select from "../components/forms/Select";
import {Link} from "react-router-dom";
import CustomersApi from "../services/CustomersApi";
import axios from "axios";
import InvoicesApi from "../services/InvoicesApi";
import {toast} from "react-toastify";
import FormContentLoader from "../components/loaders/FormContentLoader";


const InvoicePageAdd = ({history , match}) =>{
    const {id = "new"} = match.params ;
    const [invoice,setInvoice]=useState({
        amount : "",
        chrono : "",
        status : "SENT",
        sentAt : "",
        customer : ""
    })
    const [customers , setCustomers] = useState([])
    const [editing , setEditing] = useState(false);
    const [errors , setErrors] = useState({
        amount : "",
        chrono : "",
        status : "",
        sentAt : "",
        customer : ""
    })
    const [loading,setLoading] = useState(true);
    const fetchCustomers = async () => {
        try {
         const data = await CustomersApi.findAll();
         setCustomers(data);
         setLoading(false);
            if(!invoice.customer){
                setInvoice({...invoice, customer: data[0].id})
            }
        }catch (e) {
            console.log(e.response);
            toast.error("Impossible de charger les clients");
        }
    }
    useEffect(()=>{
        fetchCustomers();
    },[])

    const fetchInvoice = async (id) => {
        try {
            const {amount , chrono , status , sentAt , customer} = await InvoicesApi.find(id);
         setInvoice({amount, chrono, status, sentAt, customer: customer.id});
         setLoading(false);
        }catch (error){
            console.log(error.response)
            toast.error("Impossible de charger le facture demandé");
            history.replace("/invoices")
        }
    }

    useEffect(()=>{
        if( id !== 'new'){
            setEditing(true);
            fetchInvoice(id);
        }

    },[id]);
   const handelChange =(event)=>{
      const  {name , value} = event.currentTarget;
      setInvoice({...invoice, [name]: value});
    }

    const handleSubmit = async (event) =>{
        event.preventDefault();
        console.log(invoice);


        try{
            if(editing){
                await InvoicesApi.update(id,invoice);
                toast.success("la facture a ete bien modifie")

            }else{
                const response = await InvoicesApi.create(invoice);
                console.log(response);
                toast.success("La facture a bien ete enregistrer");
                history.replace("/invoices")

            }

        }catch (response) {
            const {violations} = response.data;
            if (violations) {
                const apiErrors = {};
                violations.forEach(({propertyPath, message}) => {
                    apiErrors[propertyPath] = message;
                });
                setErrors(apiErrors);
                toast.error("des ereurs dan votre formulaire")
            }
        }
    }

    return (
        <>

            {editing && <h1>Modification du facture</h1>  || <h1>Création d'une facture</h1>}
            {loading && <FormContentLoader/>}
            {!loading && <form onSubmit={handleSubmit}>
                <Field name="amount" type="number" placeholder="Montant de la facture" label="Montant" onChange={handelChange} value={invoice.amount} error={errors.amount}/>
                <Field name="chrono" type="number" placeholder="Montant de la facture" label="chrono" onChange={handelChange} value={invoice.chrono} error={errors.chrono}/>

                <Select name="status"  label="Statut"  value={invoice.status} error={errors.status} onChange={handelChange} >
                    <option value="SENT">Envoye</option>
                    <option value="PAID">Paye</option>
                    <option value="CANCELLED">Annulé</option>
                </Select>

                <Field name="sentAt" type="datetime-local" placeholder="Montant de la facture" label="date" onChange={handelChange} value={invoice.sentAt} error={errors.sentAt}/>
                <Select name="customer" label="Client" value={invoice.customer} error={errors.customer} onChange={handelChange}>
                    {customers.map(customer =>
                        <option key={customer.id} value={customer.id}>{customer.firstname} {customer.lastname}</option>
                    )}
                </Select>
                <div className="form-group">
                    <button type="submit" className="btn btn-success">Enregistrer</button>
                    <Link to="/invoices" className="btn btn-link">Retourner aux factures</Link>
                </div>

            </form>}
        </>
    )
}

export default InvoicePageAdd;