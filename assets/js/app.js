import React, {useContext, useState} from 'react';
import ReactDOM from 'react-dom';
import '../styles/app.css';
import Navbar from "./components/Navbar";
import Homepage from "./pages/Homapage";
import {HashRouter, Switch, Route, withRouter, Redirect} from "react-router-dom";
import CustomerPage from "./pages/CustomerPage";
import InvoicesPage from "./pages/InvoicesPage";
import LoginPage from "./pages/LoginPage";
import AuthApi from "./services/AuthApi";
import AuthContext from "./context/AuthContext";
import PrivateRoute from "./components/PrivateRoute";
import CustomerPageAdd from "./pages/CustomerPageAdd";
import InvoicePageAdd from "./pages/InvoicePageAdd";
import RegisterPage from "./pages/RegisterPage";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


console.log("hello world ???");
AuthApi.setup();


const App =()=>{
    const [isAuthenticated,setIsAuthenticated]=useState(AuthApi.isAutenticated);
    const NavWithRouter = withRouter(Navbar);
    const contextValue={
        isAuthenticated : isAuthenticated,
        setIsAuthenticated : setIsAuthenticated
    }
    return (
        <AuthContext.Provider value={contextValue}>
        <HashRouter>
            <NavWithRouter/>
            <main className="container pt-5">
                <Switch>
                    <Route path="/register" component={RegisterPage}/>
                    <Route path="/login"
                        component={LoginPage}
                    />
                        <PrivateRoute path="/invoices/:id" component={InvoicePageAdd}/>
                    <PrivateRoute path="/invoices"
                                  component={InvoicesPage}/>
                    <PrivateRoute path="/customers/:id" component={CustomerPageAdd}/>
                    <PrivateRoute path="/customers"
                                  component={CustomerPage}/>
                    <Route path="/" component={Homepage}/>
                </Switch>
            </main>
        </HashRouter>
            <ToastContainer position={toast.POSITION.BOTTOM_LEFT}/>
        </AuthContext.Provider>
    )
}

const rootElement= document.querySelector('#app');
ReactDOM.render(<App/>, rootElement);