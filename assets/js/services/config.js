export const API_URL=process.env.API_URL;
export const API_URL_LOGIN="https://localhost:8000/";

export const CUSTOMERS_API=API_URL + "customers";
export const INVOICES_API=API_URL + "invoices";
export const USERS_API=API_URL + "users";
export const LOGIN_API=API_URL_LOGIN + "auth";