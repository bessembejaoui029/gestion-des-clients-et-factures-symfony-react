import axios from "axios";
import CustomersApi from "./CustomersApi";
import {jwtDecode} from 'jwt-decode';
import {LOGIN_API} from "./config";

const logout = ()=>{
    window.localStorage.removeItem("authToken");
    delete axios.defaults.headers["Authorization"];
}
/**
*requette http d'authentification et stockage de token dans le storage et sur axios
* @param {object} credentials
*/
const AuthApi =(credentials)=>{
   return axios
        .post(LOGIN_API, credentials)
        .then(response => response.data.token)
        .then(token=>{
            window.localStorage.setItem("authToken", token);
            axios.defaults.headers["Authorization"] = "Bearer " + token;
        });
}
const setup = () => {
    const token = window.localStorage.getItem("authToken");
    if (token) {
        const jwtData = jwtDecode(token);
        if (jwtData.exp * 1000 > new Date().getTime()) {
            axios.defaults.headers["Authorization"] = "Bearer " + token;
        } else {
            logout();
        }
    }else {
        logout();
    }
}

const isAutenticated =()=>{
    const token = window.localStorage.getItem("authToken");
    if (token) {
        const jwtData = jwtDecode(token);
        if (jwtData.exp * 1000 > new Date().getTime()) {
            return true;
        }
        return false;
    }
    return false;
}

export default {
    AuthApi,
    logout,
    setup,
    isAutenticated
}