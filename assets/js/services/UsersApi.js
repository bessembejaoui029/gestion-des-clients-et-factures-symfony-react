import axios from "axios";
import {USERS_API} from "./config";

function register(user){
   return axios.post(USERS_API, user ,{
        headers : {
            'Content-Type': 'application/ld+json'
        }
    })
}

export default {
    register
}

