import axios from "axios";
import {INVOICES_API} from "./config";

function findAll(){
    return axios
        .get(INVOICES_API)
        .then(response => response.data['hydra:member']);
}

function deleteInvoices (id){
    return axios
        .delete(INVOICES_API + "/" + id);
}

function find (id){
    return axios.get(INVOICES_API + "/" + id)
        .then(response => response.data);
}
function update (id,invoice){
    return axios.put(INVOICES_API + "/" + id
        , {...invoice,customer : `/api/customers/${invoice.customer}`}
        , {
            headers: {
                'Content-Type': 'application/ld+json'
            }
        });
}

function create (invoice){
    return axios.post(INVOICES_API,
        //fel sauvegarde mtaa el invoice el customer mayethatech bel essm wlakab yelzem /api/customers/id
        //aala adheka
        //invoice.customer adhika eli mssagla fel objet  wakt eli aamlnelou submit
        {...invoice, customer: `/api/customers/${invoice.customer}`}, {
            headers: {
                'Content-Type': 'application/ld+json'
            }
        });
}

export default {
    findAll,
    delete :deleteInvoices,
    find,
    update,
    create
}