import React from "react";



const Select = ({name, label , value, error="" , onChange ,children}) =>{
    return (
        <div className="form-group">
            <label htmlFor={name} id={name}>{label}</label>
            <select name={name} id={name} className={"form-control" + ( error && " is-invalid")} value={value} onChange={onChange}>
                {/*<option value={value}>bessem bejaoui</option>
                    <option value={value}>ghofrane laajili</option>
                 */}

                {children}
            </select>
            <p className="invalid-feedback">{error}</p>
        </div>
    )
}

export default Select;