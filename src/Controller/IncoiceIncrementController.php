<?php

namespace App\Controller;

use App\Entity\Invoice;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IncoiceIncrementController extends AbstractController
{
    /**
     * @var $manager
     */
    private $manager;
    public function __construct(EntityManagerInterface $manger){
        $this->manager = $manger;
    }

    public function __invoke(Invoice $data)
    {
      $data->setChrono($data->getChrono()+ 1);
      $this->manager->flush();
      return $data;
    }
}
