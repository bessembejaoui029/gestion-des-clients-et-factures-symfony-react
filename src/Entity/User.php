<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource]
#[UniqueEntity('email',message: 'un utlisateur ayant cette adresse email existe deja')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[Assert\NotBlank(message: 'l\'email est obligatoire')]
    #[Assert\Email(message: 'le format de l\'adresse email doit etre valide')]
    #[ORM\Column(length: 180, unique: true)]
    #[Groups(['customers_read','read_invoices','invoices_subresource'])]
    private ?string $email = null;

    #[ORM\Column]
    #[Groups(['read_invoices','customers_read','invoices_subresource'])]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    #[Groups(['read_invoices','customers_read'])]
    #[Assert\NotBlank(message: 'le mot de passe est obligatoire')]
    #[Assert\Length(min: 3, max: 255, minMessage: "le mot de passe doit etre entre 3 et 255",
        maxMessage: "le mot de passe doit etre entre 3 et 255")]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_invoices','customers_read','invoices_subresource'])]
    #[Assert\NotBlank(message: 'le prenom est obligatoire')]
    #[Assert\Length(min: 3, max: 255, minMessage: "le prenom doit etre entre 3 et 255",
        maxMessage: "le prenom doit etre entre 3 et 255")]
    private ?string $firstName = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read_invoices','customers_read','invoices_subresource'])]
    #[Assert\NotBlank(message: 'le nom est obligatoire')]
    #[Assert\Length(min: 3, max: 255, minMessage: "le nom doit etre entre 3 et 255",
        maxMessage: "le nom doit etre entre 3 et 255")]
    private ?string $lastName = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Customer::class)]
    private Collection $customers;

    public function __construct()
    {
        $this->customers = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): static
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): static
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection<int, Customer>
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): static
    {
        if (!$this->customers->contains($customer)) {
            $this->customers->add($customer);
            $customer->setUser($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): static
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getUser() === $this) {
                $customer->setUser(null);
            }
        }

        return $this;
    }
}
