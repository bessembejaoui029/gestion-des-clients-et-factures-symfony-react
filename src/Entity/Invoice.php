<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use ApiPlatform\OpenApi\Model\Operation;
use App\Controller\IncoiceIncrementController;
use App\Repository\InvoiceRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;



#[ORM\Entity(repositoryClass: InvoiceRepository::class)]

#[ApiResource(
    uriTemplate: 'customers/{id}/invoices',
    operations: [new GetCollection()],
    uriVariables: [
        'id'=> new Link(
            fromProperty: 'invoices',
            fromClass: Customer::class
        )
    ],
    normalizationContext: ['groups'=>['invoices_subresource']]
)]
#[ApiResource(
    operations: [

        new GetCollection(

            //uriTemplate: '/factures'
        ),
        new Post(),
        //items operations
        new Get(
          //  uriTemplate: '/factures/{id}'
        ),
        new Put(),
        new Delete(),
        new Patch(),

       new Post(
           uriTemplate: 'invoices/{id}/increment',
           controller: IncoiceIncrementController::class,
           openapi: new Operation (
               summary: 'Increment numero facture',
               description:'![A great bear](https://picsum.photos/id/433/100/100)' . "\n"
               . '# Incrémente le chrono d\'une facture donnée de 1'
           ),
           name: 'increment'

       ),
    ],
    normalizationContext: ['groups' => ['read_invoices']],
    denormalizationContext: ["disable_type_enforcement" => true],
    order: ["sentAt" => "DESC"],
    paginationEnabled: false,
    paginationItemsPerPage: 7

)]
#[ApiFilter(
    OrderFilter::class, properties:['amount' , 'sentnAt']
)]
#[ApiFilter(
    SearchFilter::class,properties: ['customer.firstname'=>'exact'],

)]
#[ApiFilter(
    OrderFilter::class, properties:['customer.company']
)]
class Invoice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read_invoices','customers_read','invoices_subresource'])]
    private ?int $id = null;



    #[Assert\NotBlank(message: 'le montant est obligatoire')]
    #[Assert\Type(type: "numeric",message: 'le montant de la facture doit etre numeric')]
    #[ORM\Column]
    #[Groups(['read_invoices','customers_read','invoices_subresource'])]
    private ?float $amount = null;


    #[Assert\NotBlank(message: 'la date denvoi doit etre renseigne')]


   // #[Assert\DateTime(format: 'Y-m-d',message: "la date doit etre au formt YYYY-MM-DD")]
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['read_invoices','customers_read','invoices_subresource'])]
    private ?\DateTimeInterface $sentAt = null;



    #[Assert\NotBlank(message: 'la dtatus de la facture doit etre obligatoire')]
    #[Assert\Choice(choices:['SENT','PAID','CANCELLED'],message: 'le status soit SET PAID CANCELLED ')]
    #[ORM\Column(length: 255)]
    #[Groups(['read_invoices','customers_read','invoices_subresource'])]
    private ?string $status = null;


    #[Assert\NotBlank(message: 'il faut absolument chrono pour la facture')]
    #[Assert\Type(type: "numeric",message: 'le chrono doit etre numeric')]
    #[ORM\Column]
    #[Groups(['read_invoices','customers_read'])]
    private ?int $chrono = null;


    #[Assert\NotBlank(message: 'la client doit etre obligatoire')]

    #[ORM\ManyToOne(inversedBy: 'invoices')]
    #[Groups(['read_invoices'])]
    private ?Customer $customer = null;

    /**
     * requpere user a qui appartient finallement a la facture
     * @return User
     */
    #[Groups(['read_invoices','invoices_subresource'])]
    public function getUser(): User{
   return $this->customer->getUser();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount($amount): static
    {
        $this->amount = $amount;

        return $this;
    }

    public function getSentAt(): ?\DateTimeInterface
    {
        return $this->sentAt;
    }

    public function setSentAt(\DateTimeInterface $sentAt): static
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): static
    {
        $this->customer = $customer;

        return $this;
    }

    public function getChrono(): ?int
    {
        return $this->chrono;
    }

    public function setChrono(int $chrono): static
    {
        $this->chrono = $chrono;

        return $this;
    }
}
