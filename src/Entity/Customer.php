<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use function PHPUnit\TestFixture\func;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]

#[ApiResource(
  normalizationContext: ['groups' => ["customers_read"]]
)]
#[ApiFilter(
    SearchFilter::class,properties: ['firstname'=> 'partial' ,'lastname', 'company' ]
)]
#[ApiFilter(
    OrderFilter::class
)]
class Customer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["customers_read",'read_invoices'])]
    private ?int $id = null;
    #[Assert\NotBlank(message: 'le pronom est obligatoire')]
    #[Assert\Length(min: 3, max: 255, minMessage: "le prenom doit etre entre 3 et 255",
        maxMessage: "le prenom doit etre entre 3 et 255")]
    #[ORM\Column(length: 255)]
    #[Groups(["customers_read",'read_invoices'])]
    private ?string $firstname = null;


    #[Assert\NotBlank(message: 'le nom est obligatoire')]
    #[Assert\Length(min: 3, max: 255, minMessage: "le nom doit etre entre 3 et 255",
        maxMessage: "le nom doit etre entre 3 et 255")]
    #[ORM\Column(length: 255)]
    #[Groups(["customers_read",'read_invoices'])]
    private ?string $lastname = null;

    #[Assert\NotBlank(message: 'l\'email est obligatoire')]
    #[Assert\Email(message: 'le format de l\'adresse email doit etre valide')]
    #[ORM\Column(length: 255)]
    #[Groups(["customers_read",'read_invoices'])]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["customers_read",'read_invoices'])]
    private ?string $company = null;

    #[ORM\OneToMany(mappedBy: 'customer', targetEntity: Invoice::class)]
    #[Groups(["customers_read"])]

    private Collection $invoices;


    #[Assert\NotBlank(message: 'lutilisateur est obligatoire')]
    #[ORM\ManyToOne(inversedBy: 'customers')]
    #[Groups(["customers_read"])]
    private ?User $user = null;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }
    /**
     *permet de recuperer le total des invoices
     * @return float
     */
    #[Groups(['customers_read'])]
    public function getTotalAmount():float{
        return array_reduce($this->invoices->toArray(),function($total,$invoice){
            return $total+ $invoice->getAmount();
        },0);
    }
    /**
     *permet de recuperer le montant total non paye
     * @return float
     */
    #[Groups(['customers_read'])]
    public function getUnpaidAmount():float{
        return array_reduce($this->invoices->toArray(),function($total,$invoice){
            return $total+ ($invoice->getStatus()=== 'PAID' || $invoice->getStatus()=== 'CANCELLE' ? 0 : $invoice->getAmount() );
        },0);
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): static
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection<int, Invoice>
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): static
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices->add($invoice);
            $invoice->setCustomer($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): static
    {
        if ($this->invoices->removeElement($invoice)) {
            // set the owning side to null (unless already changed)
            if ($invoice->getCustomer() === $this) {
                $invoice->setCustomer(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
