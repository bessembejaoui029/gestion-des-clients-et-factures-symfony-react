<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Invoice;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $encode;
    public function __construct(UserPasswordHasherInterface $encode){
        $this->encode = $encode;
}
    public function load(ObjectManager $manager): void
    {
        $faker=Factory::create('fr_FR');

        for ($i = 0; $i < 6; $i++) {
            $user = new User();
            $chrono=1;
            $hash= $this->encode->hashPassword($user,'password');
            $user->setFirstname($faker->firstName);
            $user->setLastname($faker->lastName);
            $user->setEmail($faker->email);
            $user->setPassword($hash);
            $manager->persist($user);

            for ($i = 0; $i < mt_rand(3,5); $i++) {
                $customer = new Customer();
                $customer->setFirstname($faker->firstName);
                $customer->setLastname($faker->lastName);
                $customer->setCompany($faker->company);
                $customer->setEmail($faker->email);
                $customer->setUser($user);
                $manager->persist($customer);

                for ($i = 0; $i < mt_rand(2,4); $i++) {
                    $invoice = new Invoice();
                    $invoice->setAmount($faker->randomFloat(2,250,5000));
                    $invoice->setSentAt($faker->dateTimeBetween('-6 months'));
                    $invoice->setStatus($faker->randomElement(["SENT","PAID","CANCELLED"]));
                    $invoice->setChrono($chrono);
                    $invoice->setCustomer($customer);
                    $manager->persist($invoice);
                    $chrono++;
                }
            }
        }

        $manager->flush();
    }
}
