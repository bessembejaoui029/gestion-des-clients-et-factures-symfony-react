<?php
namespace App\EventSubscriber;

//mech l chrone uyetzed shih w wahdou
use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;
use function MyNamespace\empty1;

class InvoiceChronoSubscriber implements EventSubscriberInterface{

    public function __construct(private Security $security,private InvoiceRepository $invoiceRepository){

    }

    public static function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
        return [
          KernelEvents::VIEW => ['setChronForInvoice',EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setChronForInvoice(ViewEvent $event){
       $invoice= $event->getControllerResult();
       $method= $event->getRequest()->getMethod();
        if($invoice instanceof Invoice && $method == Request::METHOD_POST){
           $nextChrono= $this->invoiceRepository->findNextChrono($this->security->getUser());
           $invoice->setChrono($nextChrono);
            if(empty($invoice->getSentAt())){
                $invoice->setStatus(new \DateTime());
            }
        }
    }
}