<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Customer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;


class CustomerUserSubscriber implements EventSubscriberInterface{

    public function __construct(private Security $security){

    }


    public static function getSubscribedEvents()
    {
        return [

            KernelEvents::VIEW =>['setUserForCustomer',EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setUserForCustomer (ViewEvent $event){
        $customer= $event->getControllerResult();
        $method=$event->getRequest()->getMethod();
        if($customer instanceof Customer && $method == Request::METHOD_POST){
            $user=$this->security->getUser();
            $customer->setUser($user);
        }
    }
}