<?php
namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;


class PasswordEncoderSubscriber implements EventSubscriberInterface {


    public function __construct(private UserPasswordHasherInterface $encoder){

    }



    public static function getSubscribedEvents():array
    {
        // TODO: Implement getSubscribedEvents() method.
        return [
            KernelEvents::VIEW => ['encodePassword', EventPriorities::PRE_WRITE],

        ];
    }

    public function encodePassword(ViewEvent $event):void {

        $user = $event->getControllerResult();
        $method=$event->getRequest()->getMethod();
        if($user instanceof User && $method === Request::METHOD_POST){

            $hash = $this->encoder->hashPassword(
                $user,
                $user->getPassword()
            );

            $user->setPassword($hash);

        }
    }
}